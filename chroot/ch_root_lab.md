Create chrooted dir :
```
mkdir chrooted
```

Locate list of programs you want to run : ls, cat, sh
 $ whereis {program}

Find dependencies: 

ldd /usr/bin/cat
ldd /usr/bin/sh
ldd /usr/bin/ls

create dir structure inside chroot
```
mkdir -p chrooted/{usr/bin/,lib64/x86_64-linux-gnu/,lib/x86_64-linux-gnu}
```

Copy binaries and dependencies
```
cp /usr/bin/cat chrooted/user/bin/
cp /usr/bin/sh chrooted/user/bin/
...

cp /lib/x86_64-linux-gnu/libpthread.so.0 chrooted/lib/x86_64-linux-gnu/
...
cp /lib64/ld-linux-x86-64.so.2 chrooted/lib64/
```

Create text file in chroot : touch chrooted/file.txt

chroot into the dir 
```
sudo chroot ./chrooted/ /usr/bin/sh
```

Explore environment


Extra task: 

Add netcat binary to chrooted env

Extra task2 : 
Run netcat in chrooted env from specific user 
```
# chroot/etc/passwd 
root:x:0:0:root:/:/bin/sh
user:x:100:100:user:/:/bin/sh
```

sudo chroot --userspec=user  ./chroot/ /usr/bin/sh

