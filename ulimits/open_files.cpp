#include <sys/stat.h>
#include <fcntl.h>
#include <cstdlib>
#include <iostream>
#include <errno.h>
#include <string.h>

int main(int argc, char** argv) {
   	int count = std::atoi(argv[1]);
	for (int i = 0 ; i < count;i++) {
	   auto res = open("./file", O_WRONLY);
	   std::cout << i << " opened fd : " << res << std::endl;
	   if (res == -1) {
	   	std::cout <<strerror(errno) << std::endl;
	   }
	}
	return 0;
} 
