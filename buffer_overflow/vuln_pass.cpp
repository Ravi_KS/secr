#include <stdlib.h>
#include <stdio.h>
#include <string.h>
 
 
int main(int argc, char **argv) {
    char access;
    char buffer[6];
    
    access = 'n';
    strcpy(buffer, argv[1]);
 
    if (strcmp(buffer, "passwd") == 0) {
        access = 'y';
    }
 
    if (access == 'y') {
        printf("this is a secret\n");
    } else {
        printf("access denied\n");
    }
}
